//
//  ListaUsuariosTests.swift
//  ListaUsuariosTests
//
//  Created by Soto Rodriguez on 7/10/21.
//

import XCTest
import Alamofire

@testable import ListaUsuarios

class ListaUsuariosTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
        
    }
    
    func testGetUserNetworkSuccess(){
        let alamofireManager = Session.init()
        alamofireManager.request("https://jsonplaceholder.typicode.com/users", method: .get).response {
            (responseData) in switch responseData.result {
                
            case .success(_):
                XCTAssertTrue(responseData.response?.statusCode == 200, "Conexión a Api Exitoso")
                
                
            case .failure(let error):
                XCTAssertNotNil(error, "Error en el servicio")
            }
        }
        
    }
    
    func testGetUserNetworkFailed(){
        let alamofireManager = Session.init()
        alamofireManager.request("https://jsonplaceholder.typicode.com/user", method: .get).response {
            (responseData) in switch responseData.result {
                
            case .success(_):
                XCTAssertTrue(responseData.response?.statusCode != 200, "Prueba de conexion fallida Exitosa")
                
                
            case .failure(let error):
                XCTAssertNotNil(error, "Error en el servicio")
            }
        }
        
    }
    
    func testGetUserNetworkResponseSuccess(){
        let alamofireManager = Session.init()
        alamofireManager.request("https://jsonplaceholder.typicode.com/users", method: .get).response {
            (responseData) in switch responseData.result {
                
            case .success(let data):
                do {
                    let response = try JSONDecoder().decode([UserData].self, from: data!)
                    XCTAssertNotNil(response, "Repuesta valida")
                } catch  {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                XCTAssertNotNil(error, "Error en el servicio")
            }
        }
    }
    
    func testGetPostNetworkSuccess(){
        let alamofireManager = Session.init()
        alamofireManager.request("https://jsonplaceholder.typicode.com/post?userId=3", method: .get).response {
            (responseData) in switch responseData.result {
                
            case .success(_):
                XCTAssertTrue(responseData.response?.statusCode == 200, "Conexión a Api Exitoso")
            case .failure(let error):
                XCTAssertNotNil(error, "Error en el servicio")
            }
        }
        
    }
    
    func testGetUserNetworkResponseFailure(){
        let alamofireManager = Session.init()
        alamofireManager.request("https://jsonplaceholder.typicode.com/post?userId=3", method: .get).response {
            (responseData) in switch responseData.result {
                
            case .success(let data):
                do {
                    let response = try JSONDecoder().decode([UserData].self, from: data!)
                    XCTAssertNil(response, "Repuesta fallida con exito")
                } catch  {
                    print(error.localizedDescription)
                }
            case .failure(let error):
                XCTAssertNotNil(error, "Error en el servicio")
            }
        }
    }

}
