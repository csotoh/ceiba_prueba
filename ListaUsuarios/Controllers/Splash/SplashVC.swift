//
//  ViewController.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 7/10/21.
//

import UIKit

class SplashVC: UIViewController {

    @IBOutlet weak var txtVersion: UILabel!
    @IBOutlet weak var imgSplash: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        let versionNumber = (Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String) ?? "0.0"
        txtVersion.text = "Versión \(versionNumber)"
        animation()
    }
    
    func animation(){
        UIView.animate(withDuration: 2.0, animations: {() -> Void in
            self.imgSplash.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        }, completion: {(_ finished: Bool) -> Void in
            UIView.animate(withDuration: 1.0, animations: {() -> Void in
                self.imgSplash.transform = CGAffineTransform(scaleX: 1, y: 1)
                let mainQueue = DispatchQueue.main
                let deadline = DispatchTime.now() + .seconds(3)
                mainQueue.asyncAfter(deadline: deadline) {
                    self.goListUser()
                }
               
               
            })
        })
    }
    
    func goListUser(){
        performSegue(withIdentifier: "toList", sender: self)
    }


}

