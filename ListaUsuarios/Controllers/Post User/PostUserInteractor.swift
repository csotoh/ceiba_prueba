//
//  PostUserInteractor.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 8/10/21.
//

import Foundation

class PostUserInteractor: PostUserContractInteractor{
    
    var callback: PostUserContractCallback?
    let serviceHandler = ServiceHandler()
    let managerDB = CoreDataManager()
    
    func getPostUser(idUser: Int64){
        let postsUsers = managerDB.fetchAPostByUser(idUser: idUser)
        if postsUsers.isEmpty {
            self.getPostNetwork(idUser: idUser)
            return
        }
        self.callback?.listPost(listPost: postsUsers)
        
    }
    
    func setCallback(callback: PostUserContractCallback) {
        self.callback = callback
    }
    
    func getPostNetwork(idUser: Int64){
        self.serviceHandler.doGetServices(service: ServiceHandler.Service.post, parameters: ParameterPost(idUser: idUser))
        self.serviceHandler.completionHandler {(data, status, message) in
            guard status else {
                self.callback?.messageError(message: message)
                return
            }
            guard let postResponse = data as? [PostData] else{
                self.callback?.messageError(message: "Se produjo un error al procesar los datos")
                return
            }
            guard !postResponse.isEmpty else {
                self.callback?.messageError(message: "No existen registros de publicaciones del usuario")
                return
            }
            self.managerDB.addPosts(listPosts: postResponse, completion: { [weak self] in
                self!.callback?.listPost(listPost: postResponse)
            })
            
        }
    }
}
