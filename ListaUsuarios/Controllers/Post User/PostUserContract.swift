//
//  PostUserContract.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 8/10/21.
//

import Foundation

protocol PostUserContractView {
    func showPost(listPost: [PostData])
    func showMessageError(message: String)
}

protocol PostUserContractPresenter {
    
}

protocol PostUserContractInteractor {
    func getPostUser(idUser: Int64)
    func setCallback(callback: PostUserContractCallback)
}

protocol PostUserContractCallback {
    func listPost(listPost: [PostData])
    func messageError(message: String)
}
