//
//  PostUserPresenter.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 8/10/21.
//

import Foundation
import UIKit

class PostUserPresenter: PostUserContractPresenter, PostUserContractCallback {
    
    
    
    var view: PostUserContractView?
    var interactor: PostUserContractInteractor?
    
    init(view: PostUserContractView, interactor: PostUserContractInteractor, idUser: Int64){
        self.view = view
        self.interactor = interactor
        self.interactor?.setCallback(callback: self)
        self.interactor?.getPostUser(idUser: idUser)
    }
    
    func listPost(listPost: [PostData]) {
        self.view?.showPost(listPost: listPost)
    }
    
    func messageError(message: String) {
        self.view?.showMessageError(message: message)
    }
}
