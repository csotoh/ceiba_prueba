//
//  PostVC.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 8/10/21.
//

import UIKit

class PostVC: UIViewController, UITableViewDelegate, UITableViewDataSource, PostUserContractView {

    @IBOutlet weak var loadingStack: UIStackView!
    @IBOutlet weak var txtUser: UILabel!
    @IBOutlet weak var tablePost: UITableView!
    
    var presenter: PostUserContractPresenter?
    var userData: UserData?
    var listPost: [PostData] = []
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.backItem?.title = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tablePost.delegate = self
        self.tablePost.dataSource = self
        self.tablePost.estimatedRowHeight = 80
        self.tablePost.rowHeight = UITableView.automaticDimension
        
        self.txtUser.text = userData?.name
        
        self.presenter = PostUserPresenter(view: self, interactor: PostUserInteractor(), idUser: self.userData?.id ?? 0)
        
    }
    
    //MARK: View
    func showPost(listPost: [PostData]) {
        self.listPost = listPost
        self.loadingStack.isHidden = true
        self.tablePost.isHidden = false
        self.tablePost.reloadData()
    }
    
    func showMessageError(message: String) {
        showAlert(message, self)
    }
    
    func showAlert(_ msg: String, _ controller: UIViewController) {
        let alert = UIAlertController(
            title: "Atención",
            message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
    //MARK: TableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listPost.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tablePost.dequeueReusableCell(withIdentifier: "cellPost", for: indexPath) as! PostCell
        cell.initData(post: self.listPost[indexPath.row])
        return cell
        
    }
    

}
