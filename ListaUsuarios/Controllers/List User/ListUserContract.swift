//
//  ListUserContract.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 8/10/21.
//

import Foundation


protocol ListUserContractView {
    func showUsers(_listUsers: [UserData])
    func showMessageError(message: String)
}

protocol ListUserContractPresenter {
    func searchByName(searchTxt: String)
}

protocol ListUserContractInteractor {
    func getUsers()
    func searchByName(searchTxt: String)
    func setCallback(callback: ListUserContractCallback)
}

protocol ListUserContractCallback {
    func listUsers(_listUsers: [UserData])
    func messageError(message: String)
}
