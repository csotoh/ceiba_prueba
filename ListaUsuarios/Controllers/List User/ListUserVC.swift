//
//  ListUserVC.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 8/10/21.
//

import UIKit

class ListUserVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, ListUserContractView {
    
    
    @IBOutlet weak var loadingStack: UIStackView!
    @IBOutlet weak var tableUsers: UITableView!
    @IBOutlet weak var searchUsers: UISearchBar!
    
    
    var presenter: ListUserContractPresenter?
    var listUsers: [UserData] = []
    var userSelect: UserData?

    override func viewDidLoad() {
        super.viewDidLoad()

        self.searchUsers.delegate = self
        self.tableUsers.delegate = self
        self.tableUsers.dataSource = self
        self.tableUsers.estimatedRowHeight = 80
        
        self.presenter = ListUserPresenter(view: self, interactor: ListUserInteractor())
    }
    
    func showUsers(_listUsers: [UserData]) {
        self.listUsers = _listUsers
        self.loadingStack.isHidden = true
        self.tableUsers.isHidden = false
        self.tableUsers.reloadData()
    }
    
    func showMessageError(message: String) {
       showAlert(message, self)
    }
    
    func showAlert(_ msg: String, _ controller: UIViewController) {
        let alert = UIAlertController(
            title: "Atención",
            message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertAction.Style.default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
    //MARK: TableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listUsers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableUsers.dequeueReusableCell(withIdentifier: "cellUser", for: indexPath) as! UserCell
        cell.buttonPost.addTarget(self, action: #selector(buttonTouch(button:)), for: .touchDown)
        cell.buttonPost.tag = indexPath.row
        cell.initData(user: self.listUsers[indexPath.row])
        return cell
        
    }
    
    @objc func buttonTouch(button: UIButton) {
        self.userSelect = listUsers[button.tag]
        performSegue(withIdentifier: "toPosts", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toPosts"{
            let destination = segue.destination as! PostVC
            destination.userData = userSelect
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.presenter?.searchByName(searchTxt: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
}
