//
//  ListUserInteractor.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 8/10/21.
//

import Foundation

class ListUserInteractor: ListUserContractInteractor {
    
    var callback: ListUserContractCallback?
    let serviceHandler = ServiceHandler()
    let managerDB = CoreDataManager()
    
    func getUsers() {
        let allUsersDB = managerDB.fetchAllUsers()
        if allUsersDB.isEmpty {
            self.getUsersNetwork()
            return
        }
        self.callback?.listUsers(_listUsers: allUsersDB)
    }
    
    func searchByName(searchTxt: String) {
        let filterUsers = managerDB.filterNameByUser(search: searchTxt)
        self.callback?.listUsers(_listUsers: filterUsers)
    }
    
    func setCallback(callback: ListUserContractCallback) {
        self.callback = callback
    }
    
    func getUsersNetwork(){
        self.serviceHandler.doGetServices(service: ServiceHandler.Service.users, parameters: [])
        self.serviceHandler.completionHandler {(data, status, message) in
            guard status else {
                self.callback?.messageError(message: message)
                return
            }
            guard let userResponse = data as? [UserData] else{
                self.callback?.messageError(message: "Se produjo un error al procesar los datos")
                return
            }
            guard !userResponse.isEmpty else {
                self.callback?.messageError(message: "No existen registros de usuarios")
                return
            }
            self.managerDB.addUsers(listUsers: userResponse, completion: { [weak self] in
                self!.callback?.listUsers(_listUsers: userResponse)
            })
            
            
        }
    }
    
}
