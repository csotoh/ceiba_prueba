//
//  ListUserPresenter.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 8/10/21.
//

import Foundation
import UIKit

class ListUserPresenter: ListUserContractPresenter, ListUserContractCallback {

    var view : ListUserContractView?
    var interactor: ListUserContractInteractor?
    
    init(view: ListUserContractView, interactor: ListUserContractInteractor){
        self.view = view
        self.interactor = interactor
        self.interactor?.setCallback(callback: self)
        self.interactor?.getUsers()
    }
    
    //MARK: Presenter
    func searchByName(searchTxt: String) {
        if searchTxt.isEmpty {
            self.interactor?.getUsers()
        }else{
            self.interactor?.searchByName(searchTxt: searchTxt)
        }
    }
    
    //MARK: Callback
    func listUsers(_listUsers: [UserData]) {
        self.view?.showUsers(_listUsers: _listUsers)
    }
    
    func messageError(message: String) {
        self.view?.showMessageError(message: message)
    }
}
