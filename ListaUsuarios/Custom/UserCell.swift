//
//  UserCell.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 8/10/21.
//

import UIKit

class UserCell: UITableViewCell {

    @IBOutlet weak var txtEmail: UILabel!
    @IBOutlet weak var txtName: UILabel!
    @IBOutlet weak var txtPhone: UILabel!
    @IBOutlet weak var buttonPost: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func initData(user: UserData){
        txtName.text = user.name ?? ""
        txtEmail.text = user.email ?? ""
        txtPhone.text = user.phone ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
