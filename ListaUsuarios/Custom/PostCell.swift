//
//  PostCell.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 8/10/21.
//

import UIKit

class PostCell: UITableViewCell {

    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtBody: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func initData(post: PostData){
        txtTitle.text = post.title ?? ""
        txtBody.text = post.body ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
