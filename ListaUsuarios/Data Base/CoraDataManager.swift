//
//  CoraDataManager.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 8/10/21.
//

import Foundation
import CoreData

class CoreDataManager {
    
    private let container: NSPersistentContainer!
    
    init(){
        self.container = NSPersistentContainer(name: "ListUsers")
        self.setupDataBase()
    }
    
    private func setupDataBase(){
        self.container.loadPersistentStores { (desc, error) in
            guard error != nil else {return}
            print("Base de datos lista!!")
        }
    }
    
    func addUsers(listUsers: [UserData], completion: @escaping() -> Void){
        
        self.container.performBackgroundTask { (context) in
            for user in listUsers {
                let userDB = Users(context: context)
                userDB.name = user.name
                userDB.idUser = user.id
                userDB.email = user.email
                userDB.phoneNumber = user.phone
            }
            
            do{
                try context.save()
                DispatchQueue.main.async {
                    completion()
                }
            }catch{
                print("Error guardando Usuarios -> \(error)")
            }
        }
        
    }
    
    func addPosts(listPosts: [PostData], completion: @escaping() -> Void){
        
        self.container.performBackgroundTask { (context) in
            for post in listPosts {
                let postDB = Posts(context: context)
                postDB.idPost = post.id
                postDB.idUser = post.userId
                postDB.title = post.title
                postDB.body = post.body
            }
            
            do{
                try context.save()
                DispatchQueue.main.async {
                    completion()
                }
            }catch{
                print("Error guardando Posts -> \(error)")
            }
        }
        
    }
    
    func fetchAllUsers() -> [UserData] {
        
        let fetchRequest: NSFetchRequest<Users> = Users.fetchRequest()
        
        do {
            var listUsers: [UserData] = []
            let result = try container.viewContext.fetch(fetchRequest)
            for item in result {
                let user = UserData(id: item.idUser, name: item.name, email: item.email, phone: item.phoneNumber)
                listUsers.append(user)
            }
            return listUsers
            
        } catch {
            print("Error obteniendo Usuarios \(error)")
            return []
        }
        
    }
    
    func fetchAPostByUser(idUser: Int64) -> [PostData] {
        
        let fetchRequest: NSFetchRequest<Posts> = Posts.fetchRequest()
        let predicate = NSPredicate(format: "%K == %i", "idUser", idUser)
        fetchRequest.predicate = predicate
        
        do {
            var listPosts: [PostData] = []
            let result = try container.viewContext.fetch(fetchRequest)
            for item in result {
                let post = PostData(userId: item.idUser, id: item.idPost, title: item.title, body: item.body)
                listPosts.append(post)
            }
            return listPosts
            
        } catch {
            print("Error obteniendo Posts \(error)")
            return []
        }
        
    }
    
    func filterNameByUser(search: String) -> [UserData] {
        
        let fetchRequest: NSFetchRequest<Users> = Users.fetchRequest()
        let predicate = NSPredicate(format: "%K CONTAINS[c] %@", "name", search)
        fetchRequest.predicate = predicate
        
        do {
            var listUsers: [UserData] = []
            let result = try container.viewContext.fetch(fetchRequest)
            for item in result {
                let user = UserData(id: item.idUser, name: item.name, email: item.email, phone: item.phoneNumber)
                listUsers.append(user)
            }
            return listUsers
            
        } catch {
            print("Error obteniendo Usuarios \(error)")
            return []
        }
        
    }
}
