//
//  ServicesHandler.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 8/10/21.
//

import Foundation
import Alamofire

class ServiceHandler: SessionDelegate {
    
    enum Service {
        case users
        case post
    }
    
    //MARK: EndPoints
    let USERS = "/users"
    let POSTS = "/posts"
    
    typealias callback<T> = (_ data: T?, _ status: Bool, _ message : String) -> Void
    var callbackResponse: callback<Any>?
    let urlBase = "https://jsonplaceholder.typicode.com"
    var alamofireManager : Session?
    
    init(){
        super.init()
        let configuration = URLSessionConfiguration.ephemeral
        configuration.timeoutIntervalForRequest = 100
        
        alamofireManager = Session.init(configuration: configuration)
    }
    
    
    func doGetServices<T>(service: Service, parameters: T? = nil) -> Void{
        
        var urlRequest = urlBase
        var parameterRequest: Parameters = [:]
        let headers: HTTPHeaders = [
            "Content-Type": "application/json"
        ]
        
        switch service {
        case .post:
            urlRequest += POSTS
            guard let parameterPosts = parameters as? ParameterPost ?? nil else{
                return
            }
            parameterRequest = [
                "userId" : parameterPosts.idUser
            ]
            
        default:
            urlRequest += USERS
        }
        
        self.alamofireManager!.request(urlRequest, method: .get, parameters: parameterRequest, headers: headers).response {
            (responseData) in switch responseData.result {
                
            case .success(let data):
                
                switch responseData.response?.statusCode {
                case 200:
                    self.responseData(data: data, service: service)
                default:
                    self.callbackResponse?(nil, false, "Se produjo un error desconocido \(responseData.response?.statusCode ?? 0)")
                }
                
            case .failure(let error):
                self.callbackResponse?(nil, false, error.localizedDescription)
            }
        }
    }
    
    //MARK: Procesamiento del Response
    func responseData(data: Data?, service: Service){
        
        switch service {
        case .post:
            do {
                let response = try JSONDecoder().decode([PostData].self, from: data!)
                self.callbackResponse?(response as [PostData], true, "")
            } catch  {
                self.callbackResponse?(nil, false, "")
            }
        default:
            do {
                let response = try JSONDecoder().decode([UserData].self, from: data!)
                self.callbackResponse?(response as [UserData], true, "")
            } catch  {
                self.callbackResponse?(nil, false, "")
            }
        }
    }
    
    func completionHandler(callback: @escaping callback<Any>) {
        self.callbackResponse = callback
    }
}
