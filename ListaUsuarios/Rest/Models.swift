//
//  Models.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 8/10/21.
//

import Foundation


struct UserData: Decodable {
    var id: Int64
    var name: String?
    var email: String?
    var phone: String?
}

struct PostData: Decodable {
    var userId: Int64
    var id: Int64
    var title: String?
    var body: String?
}


