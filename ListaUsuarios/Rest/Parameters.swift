//
//  Parameters.swift
//  ListaUsuarios
//
//  Created by Soto Rodriguez on 8/10/21.
//

import Foundation

struct ParameterPost {
    var idUser: Int64
    
    init(idUser: Int64){
        self.idUser = idUser
    }
}
